import numpy as np


def multiply(x, y, low_sign, high_sign):
    p = 1
    m = 0
    while p <= y:
        m ^= (y & p) * x
        p *= 2
    if m & high_sign  > 0:
        m &= ~high_sign
        m ^= low_sign 
    return m
        
    
def gen_pow_matrix(primpoly):
    q = len(bin(primpoly)) - 3
    high_sign = 2**q
    low_sign = (~high_sign) & primpoly
    x = 2
    pow_matrix = -np.ones((2**q - 1, 2), dtype=int)
    
    for i in range(1, high_sign):
        pow_matrix[x - 1, 0] = i
        pow_matrix[i - 1, 1] = x
        x = multiply(x, 2, low_sign, high_sign)
    return pow_matrix


def add(x, y):
    return np.bitwise_xor(x, y)


def sum(X, axis=0):
    res = np.zeros(X.shape[0 if axis==1 else 1], dtype=int)
    if axis == 0:
        for i in X:   
            res = np.bitwise_xor(res, i)
    if axis == 1:
        for i in X.T:
            res = np.bitwise_xor(res, i)
    return res


def prod(x, y, pm):
    res = pm[x.ravel() - 1, 0] + pm[y.ravel() - 1, 0]
    res = np.remainder(res, pm.shape[0])
    res = pm[res - 1, 1]
    res = res.reshape(x.shape)
    res[x == 0] = 0
    res[y == 0] = 0
    return res


def divide(x, y, pm):
    assert(np.sum(y == 0) == 0)
    res = pm[x.ravel() - 1, 0] - pm[y.ravel() - 1, 0]
    res = np.remainder(res, pm.shape[0])
    res = pm[res - 1, 1]
    res = res.reshape(x.shape)
    res[x == 0] = 0
    return res


def linsolve(a, b, pm):
    N = a.shape[0]
    ab = np.hstack((a, b.reshape(-1, 1)))
    
    for i in range(N):
        for j in range(i, N):
            if ab[j, i] != 0:
                if i != j:
                    buf = ab[j, :].copy()
                    ab[j, :] = ab[i, :].copy()
                    ab[i, :] = buf
                break
            if j == N - 1:
                return np.nan
        
        tmp = ab[i, i] * np.ones(ab.shape[1], dtype=int)
        ab[i, :] = divide(ab[i, :], tmp, pm)
        if i == N - 1:
            break
        tmp = np.tile(ab[i + 1:, i], ab.shape[1]).reshape(ab.shape[1], N - i - 1).T
        tmp2 = np.tile(ab[i, :], N - i - 1).reshape(N - i - 1, ab.shape[1])
        ab[i + 1:,:] = add(ab[i+1:, :], prod(tmp, tmp2, pm))
    
    for i in range(N - 1, 0, -1):
        tmp = np.tile(ab[:i, i], ab.shape[1]).reshape(ab.shape[1], i).T
        tmp2 = np.tile(ab[i, :], i).reshape(i, ab.shape[1])
        ab[:i, :] = add(ab[:i, :], prod(tmp, tmp2, pm))
    
    return ab[:, -1]

def minpoly(x, pm):
    used_elem = np.zeros(pm.shape[0] + 1, dtype=int)
    old_n = -1
    new_n = 0
    tmp_x = x.copy()
    
    while old_n != new_n:
        used_elem[tmp_x] = 1
        tmp_x = prod(tmp_x, tmp_x, pm)
        old_n = new_n
        new_n = np.sum(used_elem)
    
    res_x = np.arange(pm.shape[0] + 1)[used_elem == 1]
    indx = np.arange(res_x.shape[0] + 1)
    res_poly = np.zeros(res_x.shape[0] + 1, dtype=int)
    
    for i in range(res_x.shape[0]):
        if i == 0:
            res_poly[-1] = res_x[i]
            res_poly[-2] = 1
        else:
            buf = np.zeros(res_x.shape[0] + 1, dtype=int)
            buf[:-1] = res_poly[1:].copy()

            tmp = res_x[i] * np.ones(res_x.shape[0] + 1, dtype=int) 
            res_poly = prod(res_poly, tmp, pm)
            res_poly = add(res_poly, buf)
    
    return (res_poly, res_x)
            
    
def polyval(p, x, pm):
    xpows = np.ones((x.shape[0], p.shape[0]), dtype=int)
    tmp = x.copy()
    for i in range(p.shape[0] - 2, -1, -1):
        xpows[:, i] = tmp
        tmp = prod(tmp, x, pm)
    ptmp = np.tile(p, x.shape[0]).reshape(x.shape[0], -1)

    res = prod(ptmp, xpows, pm)
    res = sum(res, axis=1)
    return res


def polyprod(p1, p2, pm):
    N = p1.shape[0] + p2.shape[0] - 1
    res = np.zeros(N, dtype=int)
    for i in range(p2.shape[0]):
        tmp = np.zeros(N, dtype=int)
        tmp[i: i + p1.shape[0]] = p1.copy()
        tmp2 = p2[i] * np.ones(N, dtype=int)
        res = add(res, prod(tmp, tmp2, pm))
        
    return res


def polydivmod(p1, p2, pm):
    if p2.shape[0] > p1.shape[0]:
        return(np.array([0]), p1.copy())
    
    a1 = p1.copy()
    a2 = np.zeros(a1.shape[0], dtype=int)
    a2[:p2.shape[0]] = p2.copy()
    tmp_main = a2[0] * np.ones(a2.shape[0], dtype=int)
    a2 = divide(a2, tmp_main, pm)
    
    res_div = np.zeros(p1.shape[0] - p2.shape[0] + 1, dtype=int)
    
    for i in range(res_div.shape[0]):
        res_div[i] = a1[0]
        tmp = a1[0] * np.ones(a2.shape[0], dtype=int)
        a1 = add(a1, prod(a2, tmp, pm))
        a1 = a1[1:]
        a2 = a2[:-1]
    
    
    res_div = divide(res_div, tmp_main[:res_div.shape[0]], pm)
    if np.sum(a1) == 0:
        return(res_div, np.array([0]))
    
    return(res_div, a1[np.nonzero(a1)[0][0]:])


def polyadd(p1, p2):
    max_shape = max(p1.shape[0], p2.shape[0])
    res = np.zeros(max_shape, dtype=int)
    
    if p1.shape[0] >= p2.shape[0]:
        res[-p2.shape[0]:] = p2
        res = add(res, p1)
    else:
        res[-p1.shape[0]:] = p1
        res = add(res, p2)
    
    if np.sum(res) == 0:
        return np.array([0])
    
    return res[np.nonzero(res)[0][0]:]


def euclid(p1, p2, pm, max_deg=0):
    r_old = p1
    r_new = p2
    
    x_old = np.array([1])
    y_new = np.array([1])
    
    x_new = np.array([0])
    y_old = np.array([0])
    while(np.sum(r_new) > 0 and (max_deg == 0 or r_old.shape[0] - 1 > max_deg)):
        (q_tmp, r_tmp) = polydivmod(r_old, r_new, pm)
        r_old = r_new.copy()
        r_new = r_tmp.copy()
        x_tmp = polyadd(x_old, polyprod(q_tmp, x_new, pm))
        y_tmp = polyadd(y_old, polyprod(q_tmp, y_new, pm))
        
        
        x_old = x_new
        y_old = y_new
        x_new = x_tmp
        y_new = y_tmp

    
    return (r_old, x_old, y_old)
