import numpy as np
from numpy.lib.stride_tricks import as_strided
from gf import *

def genpoly(n, t):
    polynoms = [7,11,19,37,67,131,285,529,1033,2053,4179,8219,16427,32771,65581]
    l = int(np.log2(n + 1))
    primpoly = polynoms[l - 2]
    pm = gen_pow_matrix(primpoly)
    roots = pm[:2*t,1]
    minpol = minpoly(roots, pm)
    return minpol[0], roots, pm


def dist(g, n):
    m = g.shape[0] - 1
    k = n - m
    U = np.zeros((2 ** k, k), np.int)
    for i in range(2 ** k):
        for j in range(k):
            if i & (1 << j):
                U[i, j] = 1
    V = np.sum(coding(U, g), axis=1)[1:]
    return np.min(V)


def coding(U, g):
    pm = np.array([[1, 1]])
    m = g.shape[0] - 1
    V = np.zeros((U.shape[0], U.shape[1] + m), dtype=np.int)
    for i, u in enumerate(U):
        xmu = np.zeros(m + u.shape[0], dtype=np.int)
        xmu[0:u.shape[0]] = u
        div, mod = polydivmod(xmu, g, pm)
        xmu[-mod.shape[0]:] = mod
        V[i] = xmu
    return V


def decoding(W, R, pm, method='euclid'):
    dW = W.copy().astype(np.float32)
    t = R.shape[0] // 2
    for i, w in enumerate(W):
        s = np.zeros(R.shape[0], np.int)
        s = polyval(w, R[::-1], pm)
        if np.sum(s) == 0:
            continue
        if method == 'euclid':
            x2t_1 = np.zeros(R.shape[0] + 2, np.int)
            x2t_1[0] = 1
            r, a, sigma = euclid(x2t_1, np.append(s, 1), pm, t)
            vals = W.shape[1] - 1 - ((-(np.where(polyval(sigma, pm[:, 1], pm) == 0)[0] + 1)) % W.shape[1])
            if vals.shape[0] != sigma.shape[0] - 1:
                dW[i] = np.nan
            else:
                dW[i, vals] = (dW[i, vals] + 1) % 2
        if method == 'pgz':
            s = s[::-1].copy()
            for v in range(t, 0, -1):
                A = as_strided(s, strides=(s.itemsize,s.itemsize), shape=(v, v))
                b = s[v:2*v]
                sigma = linsolve(A, b, pm)
                if sigma is not np.nan:
                    break
            if sigma is np.nan:
                dW[i] = np.nan
                continue
            sigma = np.append(sigma, 1)
            vals = W.shape[1] - 1 - ((-(np.where(polyval(sigma, pm[:, 1], pm) == 0)[0] + 1)) % W.shape[1])
            dW[i, vals] = (dW[i, vals] + 1) % 2
            s = polyval(dW[i].astype(np.int), R, pm)
            if np.sum(s) != 0:
                dW[i] = np.nan
    return dW
