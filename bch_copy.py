import numpy as np
import gf

def polybitmod(x, y):
    res = x.copy()
    for i in range(x.shape[0] - y.shape[0] + 1):
        if res[i] == 1:
            res[i: i + y.shape[0]] = np.bitwise_xor(res[i: i + y.shape[0]], y)
    return res[-y.shape[0] + 1:]


def coding(U, g):
    m = g.shape[0] - 1
    k = U.shape[1]
    res = np.zeros((U.shape[0], k + m), dtype=int)
    x_m = np.zeros(g.shape[0] + 1, dtype=int)

    for i, elem in zip(range(U.shape[0]), U):
        tmp =  np.zeros(k + m, dtype=int)
        tmp[:k] = elem
        tmp[k:] = polybitmod(tmp, g)
        res[i] = tmp

    return res


def dist(g, n):
    k = n - g.shape[0] + 1
    pows = np.zeros(n, dtype=int)
    pows[k - 1] = 1
    for i in range(k - 2, -1, -1):
        pows[i] = 2 * pows[i + 1]
    dist_min = n

    for i in range(1, 2 ** k):
        tmp = i * np.ones(n, dtype=int)
        u = np.bitwise_and(tmp, pows)
        u[u > 0] = 1
#         u[-g.shape[0] + 1:] = polybitmod(u, g)

        dist_tmp = np.sum(u)
        dist_tmp += np.sum(polybitmod(u, g))
        dist_min = min(dist_tmp, dist_min)

    return dist_min


def generate_small_data(data):
    q = np.arange(2, 17)
    q = 4 * np.ones(15, dtype=int)
    for i in range(1, q.shape[0]):
        q[i] = 2*q[i - 1]
    primpoly = np.zeros(q.shape[0], dtype=int)
    j = 0
    for i in data:
        if i & q[j] > 0:
            primpoly[j] = i
            j += 1
        if j == q.shape[0]:
            break
    return primpoly


def genpoly(n, t):
    d = 2 * t + 1
    q = int(np.log2(n + 1))
    assert(n == 2**q - 1 and d <= n)
    small_data = np.array([7, 11, 19, 37, 67, 131, 285, 529,  1033,  2053,  4179,  8219,
                                                 16427, 32771, 65581])
    primpoly = small_data[q - 2]
    pm = gf.gen_pow_matrix(primpoly)
    x = 2 * np.ones(d, dtype=int)
    zeros_bch = pm[:d-1, 1]
    respoly = gf.minpoly(zeros_bch, pm)[0]

    return(respoly, zeros_bch, pm)


def sindromlinsolve(sindrom, pm):
    d = sindrom.shape[0]

    for t in range(d // 2, 0, -1):
        A = np.zeros((t, t), dtype=int)
        for i in range(t):
            A[i, :] = sindrom[i: i + t]
        b = sindrom[t: 2 * t]
        ans = gf.linsolve(A, b, pm)
        if np.isnan(ans).all() == False:
            ans = np.concatenate((ans, np.array([1])))
            return ans
    return np.nan


def finderror(p, pm):
    res_poly = gf.polyval(p, pm[:, 1], pm)
    indx_error = np.arange(pm.shape[0], dtype=int) + 1
    indx_error = indx_error[res_poly == 0]
    indx_error = pm.shape[0] - indx_error
    indx_error = np.remainder(indx_error, pm.shape[0])
    indx_error = pm.shape[0] - indx_error - 1

    return indx_error


def sindromeuclid(sindrom, pm):
    s = np.concatenate((sindrom[-1::-1], np.array([1])))
    t = sindrom.shape[0] // 2
    s = s[np.nonzero(s)[0][0]:]
    z = np.zeros(2 * t + 2, dtype=int)
    z[0] = 1

    r, p1, p2 = gf.euclid(z, s, pm, t)
    print(z, s, t)
    res_poly = gf.polyval(p2, pm[:, 1], pm)

    if np.sum(res_poly == 0) != p2.shape[0] - 1:
        return np.nan

    return p2


def decoding(W, R, pm, method='euclid'):
    n = W.shape[1]
    gpoly = gf.minpoly(R, pm)[0]
    k = n - gpoly.shape[0] + 1
    res = np.zeros((W.shape[0], k), dtype=float)

    for i in range(W.shape[0]):
        p = W[i, :].copy()
        sindrom = gf.polyval(p, R, pm)
        if np.sum(sindrom) == 0:
            res[i, :] = p[:k]
            continue

        if method == 'pgz':
            polyerror = sindromlinsolve(sindrom, pm)
        if method == 'euclid':
            polyerror = sindromeuclid(sindrom, pm)
        # print(sindrom)
        if np.isnan(polyerror).all() == True:
            res[i, :] = np.nan * np.ones(k, dtype=int)
            continue

        indxerror = finderror(polyerror, pm)
        p = p.astype(np.bool)
        p[indxerror] = np.bitwise_not(p[indxerror])
        p = p.astype(int)

        sindrom = gf.polyval(p, R, pm)
        if np.sum(sindrom) > 0:
            res[i, :] = np.nan * np.ones(k, dtype=int)
            continue

        res[i, :] = p[:k]

    return res

