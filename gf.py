import numpy as np

def gen_pow_matrix(primpoly):
    br = np.binary_repr(primpoly);
    q = len(br) - 1
    l = 2 ** q
    pm = np.empty((l - 1, 2), dtype=np.int)
    move_factor = primpoly ^ (2 ** q)
    alpha = 2
    for i in range(l - 1):
        pm[i,1] = alpha
        pm[alpha-1,0] = i+1
        alpha <<= 1
        if alpha & (2 ** q):
            alpha ^= primpoly
    return pm


def add(X, Y):
    if len(X.shape) == 1 and len(Y.shape) == 1 and X.shape != Y.shape:
        max_s = max(X.shape[0], Y.shape[0])
        return np.bitwise_xor(np.pad(X, [max_s - X.shape[0], 0], 'constant'),
                              np.pad(Y, [max_s - Y.shape[0], 0], 'constant'))
    return np.bitwise_xor(X, Y)


def sum(X, axis=0):
    return np.bitwise_xor.reduce(X, axis=axis)


def prod(X, Y, pm):
    tmp = np.array(pm[(pm[X - 1,0] + pm[Y - 1,0] - 1) % pm.shape[0], 1])
    tmp[np.logical_or(X == 0, Y == 0)] = 0
    return tmp


def divide(X, Y, pm):
    # beware of unsafe division to zero!
    tmp = np.array(pm[(pm[X - 1,0] - pm[Y - 1,0] - 1) % pm.shape[0], 1])
    tmp[np.logical_or(X == 0, Y == 0)] = 0
    return tmp


def linsolve(A, b, pm):
    Ac = np.hstack((A, b[:,np.newaxis]))
    for i in range(A.shape[1]):
        j = i
        while j < A.shape[0] and Ac[j, i] == 0:
            j += 1
        if j == Ac.shape[0]:
            return np.nan
        Ac[[i, j]] = Ac[[j, i]]
        Ac[i] = divide(Ac[i], Ac[i, i], pm)
        for j in range(i + 1, A.shape[0]):
            if Ac[j, i] != 0:
                Ac[j] = add(prod(Ac[i], Ac[j, i], pm), Ac[j])
    for i in range(A.shape[0] - 1, -1, -1):
        for j in range(i - 1, -1, -1):
            if Ac[j, i] != 0:
                Ac[j] = add(prod(Ac[i], Ac[j, i], pm), Ac[j])
    return Ac[:,-1]


def polyval(p, x, pm):
    deg = np.arange(p.shape[0])[::-1]
    return sum(prod(pm[(pm[x[:,np.newaxis] - 1, 0] * deg - 1) % pm.shape[0], 1], p, pm), axis=1)


def polyprod(p1, p2, pm):
    ans = np.zeros(p1.shape[0] + p2.shape[0] - 1, dtype=np.int)
    for i in range(ans.shape[0]):
        j = 0
        while i - j >= 0 and j < p1.shape[0]:
            if i - j >= p2.shape[0]:
                j += 1
                continue
            ans[i] = add(ans[i], prod(p1[j], p2[i - j], pm))
            j += 1
    return ans


def minpoly(x, pm):
    alps = np.zeros(pm.shape[0])
    degs = pm[x - 1, 0]
    for i in range(pm.shape[0]):
        alps[degs - 1] = 1
        degs = (degs * 2) % pm.shape[0]
    res = np.array([1])
    roots = np.empty(0, dtype=np.int)
    for i in range(alps.shape[0]):
        if alps[i] == 0:
            continue
        roots = np.append(roots, pm[i, 1])
        res = polyprod(res, np.array([1, pm[i, 1]]), pm)
    return res, roots


def polydivmod(p1, p2, pm):
    div = np.zeros(p1.shape[0] - p2.shape[0] + 1, dtype=np.int)
    mod = p1.copy()
    for i in range(p1.shape[0]):
        if p1.shape[0] - i < p2.shape[0]:
            break
        if mod[i] == 0:
            continue
        d = divide(mod[i], p2[0], pm)
        mod = add(mod, np.pad(prod(p2, d, pm), [i, p1.shape[0] - p2.shape[0] - i], 'constant'))
        div[i] = d
    mod = np.trim_zeros(mod, 'f')
    if div.shape[0] == 0:
        div = np.array([0], np.int)
    if mod.shape[0] == 0:
        mod = np.array([0], np.int)
    return div, mod


def euclid(p1, p2, pm, max_deg=0):
    a, b = p1, p2
    x = np.zeros(1, np.int)
    y = np.ones(1, np.int)
    q, r = polydivmod(a, b, pm)
    while r.shape[0] - 1 > max_deg:
        if r.shape[0] == 1 and r[0] == 0:
            break
        a, b = b, r
        x, y = y, np.trim_zeros(add(x, polyprod(y, q, pm)), 'f')
        q, r = polydivmod(a, b, pm)
    x, y = y, np.trim_zeros(add(x, polyprod(y, q, pm)), 'f')
    return r, x, y

